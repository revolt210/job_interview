-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 15 2017 г., 02:26
-- Версия сервера: 5.5.50
-- Версия PHP: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `db_name`
--

-- --------------------------------------------------------

--
-- Структура таблицы `table1`
--

CREATE TABLE IF NOT EXISTS `table1` (
  `id` int(11) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `name` varchar(25) NOT NULL,
  `email` varchar(50) NOT NULL,
  `text` text NOT NULL,
  `file` varchar(255) NOT NULL,
  `date_time` time NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `table1`
--

INSERT INTO `table1` (`id`, `user_name`, `name`, `email`, `text`, `file`, `date_time`, `date`) VALUES
(1, 'login1', 'Павел', 'pasharudenko@ukr.net', 'texttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttext', '15_03_2017_02_14_43.jpg', '02:14:43', '2017-03-15'),
(2, 'login2', 'Виктор', 'viktor@mail.ru', 'texttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttext', '15_03_2017_02_15_56.jpg', '02:15:56', '2017-03-15'),
(3, 'login3', 'Максим', 'maxim@mail.ru', 'texttexttexttexttexttexttexttexttexttexttext', '15_03_2017_02_17_28.jpg', '02:17:28', '2017-03-15');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `table1`
--
ALTER TABLE `table1`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `table1`
--
ALTER TABLE `table1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
